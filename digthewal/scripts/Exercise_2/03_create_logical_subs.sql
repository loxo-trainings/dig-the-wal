-- This is the script for the logical replicating database.
DROP DATABASE IF EXISTS publishdb;
CREATE DATABASE publishdb;
\c publishdb
DROP SUBSCRIPTION IF EXISTS pub_table_sub;
DROP TABLE IF EXISTS pub_table;

CREATE TABLE pub_table(id bigserial PRIMARY KEY, data text);
\echo "Before subscription"
select lsn,pg_walfile_name(lsn) from  pg_current_wal_lsn() lsn(lsn);
\echo Connecting :pubport
CREATE SUBSCRIPTION pub_table_sub CONNECTION 'host=127.0.0.1 port=':'pubport''  user=replication password=test dbname=publishdb' PUBLICATION pub_table_pub;
\echo "After subscription"

