-- This is the script for the replicated database
DROP DATABASE IF EXISTS publishdb;
CREATE DATABASE publishdb;
\c publishdb
CREATE TABLE IF NOT EXISTS pub_table(id bigserial PRIMARY KEY, data text);
INSERT INTO pub_table SELECT x, md5(''||random()*extract(epoch from current_timestamp)) FROM generate_series(1,1000) as x(x);

-- Hey I know this is not secure as a password but it's only VM test purpose right ?
DROP USER IF EXISTS replication;
CREATE USER replication WITH REPLICATION ENCRYPTED PASSWORD 'test' LOGIN;
GRANT SELECT ON pub_table TO replication;

-- Create publication for this table
CREATE PUBLICATION pub_table_pub FOR TABLE pub_table;
