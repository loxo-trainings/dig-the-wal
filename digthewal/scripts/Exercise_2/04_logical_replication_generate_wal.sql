-- This script aims so simulate db creation C(nor R)UD activity then db drop
\set QUIET on
\a
\x
\t
DELETE FROM pub_table;
SELECT pg_current_wal_lsn() as start_lsn \gset

INSERT INTO pub_table SELECT x, md5(''||random()*extract(epoch from current_timestamp)) FROM generate_series(1,1000) as x(x);
SELECT pg_current_wal_lsn() as delete_lsn \gset
DELETE FROM pub_table WHERE id = 500;
UPDATE pub_table SET data = 'test' WHERE id=600;

SELECT pg_current_wal_lsn() as stop_lsn \gset
select pg_walfile_name(:'start_lsn') as startwal \gset
select pg_walfile_name(:'stop_lsn') as stopwal \gset
select pg_walfile_name(:'delete_lsn') as deletewal \gset
select setting as clustername from pg_settings where name='data_directory' \gset
\echo #--------------------------------- ON PRIMARY -------------
\echo # whole WAL decoded
\echo /usr/lib/postgresql/11/bin/pg_waldump -s :start_lsn  -e :stop_lsn -b -p :'clustername' :startwal :stopwal
\echo # delete to the end WAL decoded
\echo /usr/lib/postgresql/11/bin/pg_waldump -s :delete_lsn -e :stop_lsn  -b -p :'clustername' :deletewal :stopwal
\echo #----------------------------------------------------------
