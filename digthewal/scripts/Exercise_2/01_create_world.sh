# Create DATABASE on logical
pg_dropcluster 11 logicalprimary --stop
pg_dropcluster 11 logicalsecondary --stop
pg_createcluster 11 logicalprimary -o  wal_level=logical --start -- -k
pg_createcluster 11 logicalsecondary -o  wal_level=logical --start -- -k
LOGICALPUBPORT=$(pg_lsclusters -h 11 logicalprimary|awk '{print $3}')
LOC=$(dirname $0)

psql --cluster 11/logicalprimary  -f ${LOC}/02_create_logical_repl.sql
psql --cluster 11/logicalsecondary  --set pubport=${LOGICALPUBPORT} -f ${LOC}/03_create_logical_subs.sql
psql --cluster 11/logicalprimary  publishdb -f ${LOC}/04_logical_replication_generate_wal.sql
echo "AFTER wal generation"
echo "Publisher"
psql --cluster 11/logicalprimary  -c 'select lsn,pg_walfile_name(lsn) from  pg_current_wal_lsn() lsn(lsn);'
echo "Subscribers"
psql --cluster 11/logicalsecondary  -c 'select lsn,pg_walfile_name(lsn) from  pg_current_wal_lsn() lsn(lsn);'
