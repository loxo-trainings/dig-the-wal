#! /bin/bash
# Drop previously created cluster if any
pg_dropcluster 11 crashable --stop
# Create the cluster and start it
pg_createcluster 11 crashable --start -- -k
## Execute some transaction giving some information on lsn/WAL files
## Note that some files are create with LSN/Walfiles informations
cd
psql --cluster 11/crashable -c "select name,setting from pg_settings where name = 'wal_level'"
psql --cluster 11/crashable << EOF
DROP DATABASE IF EXISTS crashable;
CREATE DATABASE crashable;
\c crashable
CREATE TABLE data(id bigserial primary key, ddate timestamptz);
BEGIN WORK;
INSERT INTO data(ddate) VALUES(current_timestamp);
INSERT INTO data(ddate) VALUES(current_timestamp);
INSERT INTO data(ddate) VALUES(current_timestamp);
COMMIT;

select lsn,pg_walfile_name(lsn) walfile FROM pg_current_wal_lsn() as lsn(lsn);
CHECKPOINT;

select pg_sleep(2);
BEGIN WORK;
INSERT INTO data(ddate) VALUES(current_timestamp);
INSERT INTO data(ddate) VALUES(current_timestamp);
INSERT INTO data(ddate) VALUES(current_timestamp);
INSERT INTO data(ddate) VALUES(current_timestamp);
COMMIT;

\a
\t
\o current_pre_crash_walfile.txt
select lsn,pg_walfile_name(lsn) walfile FROM pg_current_wal_lsn() as lsn(lsn);
\o
\a
\t
select pg_sleep(2);
BEGIN WORK;
INSERT INTO data(ddate) VALUES(current_timestamp);
INSERT INTO data(ddate) VALUES(current_timestamp);
COMMIT;
\a
\t
\o current_crash_walfile.txt
select setting||'/pg_wal/'||pg_walfile_name(pg_current_wal_lsn()) from pg_settings where name ='data_directory';
\o
\a
\t
SELECT * FROM data;
EOF
echo "================= CRASH MAIN PROCESS ========================"
## pg_ctl stop -m immediate  will prevent PostgreSQL from checkpointing the WAL
## PG will still relaese resources such as shared_buffers and lock files
##
## We could have performed kill -9 $(cat /var/run/postgresql/11-crashable.pid) 
## but you MUST avoid doing such : resources like shared buffers and 
## lock files will persist after server crash.
## OTOH you may be aware that in real situation, server crash 
## often don't release such resources so you should take care of using
## ipcs to inspect them.
##
## If you have to stop a postgresql engine, you MUST first use pg_ctl
pg_ctlcluster 11 crashable stop -m immediate

## Retrieve walfile name
WALFILE=$(cat current_crash_walfile.txt)
rm current_crash_walfile.txt
## Save file before corruption
echo "WALFILE : ${WALFILE}"
echo "Original file is in ${WALFILE}.old"
cp ${WALFILE} ${WALFILE}.old
echo "WAL file corruption -------------"
#exec 3<> ${WALFILE} && echo $(echo -e -n "\xDE\xAD" && cat ${WALFILE} && echo -e -n "\xBE\xEF") >&3
(echo -e -n "\xDE\xAD" && cat ${WALFILE} && echo -e -n "\xBE\xEF") > ${WALFILE}.corr
mv ${WALFILE}.corr ${WALFILE}
chmod 600 ${WALFILE}
echo "Hex output @ content : Should start with 0000 dead"
od -x -w2 --endian=big ${WALFILE} -N 2
ls -l ${WALFILE}*
## Restart server if possible
echo "Trying to restart with corrupted WAL file"
pg_ctlcluster 11 crashable start

echo "What to do now cluster is broken ?"
cd -
