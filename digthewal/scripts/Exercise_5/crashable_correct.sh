# Cleanup with pg_resetwal
CRASHPORT=$(pg_lsclusters 11 crashable -h|awk '{print $3}')
/usr/lib/postgresql/11/bin/pg_resetwal -D 11/crashable -n
/usr/lib/postgresql/11/bin/pg_resetwal -D 11/crashable -f
pg_ctlcluster 11 crashable start
tail -20 /var/log/postgresql/postgresql*crashable*
psql -p ${CRASHPORT} crashable -c "select lsn, pg_walfile_name(lsn) from pg_current_wal_lsn() as lsn(lsn)"
psql -p ${CRASHPORT} crashable -c "select * from data"
