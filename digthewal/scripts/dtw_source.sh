#! /bin/bash

pgroot="/var/lib/postgresql"
scriptroot="${pgroot}/digthewal/scripts"
wal_root="${pgroot}/wal_archives"
backup_root="${pgroot}/backups"
