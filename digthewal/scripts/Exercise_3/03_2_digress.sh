#! /bin/bash

psql --cluster 11/replica_secondary  -c 'select pg_wal_replay_pause()';
psql --cluster 11/replica_secondary importantdb -c 'select * from importantlog order by id limit 5';

psql --cluster 11/replica_primary -f ~/digthewal/scripts/Exercise_3/03_1_create_importantdb.sql

psql --cluster 11/replica_secondary importantdb -f ~/digthewal/scripts/Exercise_3/03_2_digress.sql

