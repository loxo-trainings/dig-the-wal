#! /bin/bash
source $(dirname $0)/../dtw_source.sh
primary_wal="${wal_root}/11/replica_primary"
secondary_wal="${wal_root}/11/replica_secondary"
echo "================ EXERCISE 3 : CLEANUP ======================="
pg_dropcluster 11 replica_primary --stop
pg_dropcluster 11 replica_secondary --stop


echo "================ EXERCISE 3 : INITIALIZATION ================"
# Replica primary level
echo "Creating ${primary_wal}"
mkdir -p ${primary_wal}
echo "Creating ${backup_root}/11/replica_primary"
mkdir -p ${backup_root}/11/replica_primary
pg_createcluster 11 replica_primary -o archive_mode='on' \
  -o archive_command="cp %p ${primary_wal}/%f" \
  -- -k
# We need a specific pg_hba.conf (replication)
cp ${scriptroot}/system/pg_hba.conf /etc/postgresql/11/replica_primary/
pg_ctlcluster 11 replica_primary start

# Replica primary level
echo "Creating ${secondary_wal}"
mkdir -p ${secondary_wal}
echo "Creating ${backup_root}/11/replica_secondary"
mkdir -p ${backup_root}/11/replica_secondary
pg_createcluster 11 replica_secondary  \
  --start -- -k

echo "================ EXERCISE 3 : INITIALIZED ================"

PRIMPORT=$(pg_lsclusters  11 replica_primary -h|awk '{print $3}') 
echo "creating replication user"
psql --cluster 11/replica_primary -c "CREATE USER replication WITH replication PASSWORD 'test';"
echo "stopping secondary"
pg_ctlcluster 11 replica_secondary stop
echo "backuping primary with pg_basebackup"
rm -rf ~/11/replica_secondary/*
/usr/lib/postgresql/11/bin/pg_basebackup -P -p${PRIMPORT} --wal-method=stream -R -D ~/11/replica_secondary/
echo "creating recovery.conf"
cat > ~/11/replica_secondary/recovery.conf << EOF  
primary_conninfo='port=${PRIMPORT} user=replication password=test'
restore_command='cp /var/lib/postgresql/wal_archives/11/replica_primary/%f %p'
standby_mode='on'
EOF
echo "Starting secondary"
pg_ctlcluster 11 replica_secondary start
echo "Fill primary with data, then create some disasters"
psql --cluster 11/replica_primary  -f ~/digthewal/scripts/Exercise_3/03_1_create_importantdb.sql


