select pg_is_wal_replay_paused();
select * from importantlog order by id limit 5;
\c postgres
select pg_last_wal_receive_lsn();
select pg_last_wal_replay_lsn();
select pg_wal_replay_resume();
select pg_sleep(5);
\c importantdb
select * from importantlog order by id limit 5;
