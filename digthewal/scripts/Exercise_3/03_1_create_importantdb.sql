DROP DATABASE IF EXISTS importantdb;
CREATE DATABASE importantdb;
\c importantdb
CREATE TABLE importantlog(
	id bigserial primary key,
	logtime timestamp with time zone not null default current_timestamp,
	logtext text not null);

CREATE TABLE witnesstable(data text NOT NULL);


-- Let's insert some data into the log
INSERT INTO importantlog(logtext)
	select noun||' '||verb||' '||what
	FROM (VALUES ('Alice'),('Bob'),('John'),('Jasmine'),('Martin')) as nouns(noun)
	CROSS JOIN  (VALUES ('chops'),('shops'),('reads'),('codes')) as verbs(verb)
	CROSS JOIN (VALUES ('book'),('wood'),('program'),('food')) as whats(what) ORDER BY random();

-- And good data into witnesstable
INSERT INTO witnesstable VALUES ('Good data 1'),('Good data 2');

-------------------------------------------------------------
-- Start a backup now ---------------------------------------
--select pg_switch_wal();
--select pg_start_backup('loxobackup'||current_timestamp);
--
select setting as clustername from pg_settings where name='cluster_name' \gset
select setting as clusterport from pg_settings where name='port' \gset
\o /tmp/back.sh
\t
\a
-- Yes I know it's not clean but it works quite well
select 'pg_basebackup -P -p'||:'clusterport'||' -R -D ~/backups/'||:'clustername'||'/`date -Is` --wal-method=stream';
\o
\! sh /tmp/back.sh
--select pg_stop_backup();
-- Backup finished -----------------------------------------


-- INSERT others values
INSERT INTO importantlog(logtext)
	select noun||' '||verb||' '||what
	FROM (VALUES ('Jean'),('Gill'),('Renata'),('Paul'),('Nicole')) as nouns(noun)
	CROSS JOIN  (VALUES ('chops'),('shops'),('reads'),('codes')) as verbs(verb)
	CROSS JOIN (VALUES ('book'),('wood'),('program'),('food')) as whats(what) ORDER BY random() LIMIT 80*random();
\echo Some help before delete
select pg_current_wal_lsn(),pg_walfile_name(pg_current_wal_lsn()) as before_delete_lsn;
select pg_current_wal_insert_lsn(),pg_walfile_name(pg_current_wal_insert_lsn()) as before_delete_insert_lsn;
-- Ooops #1
DELETE FROM importantlog WHERE id < 50;
select pg_switch_wal();
\echo Some help before drop
select pg_current_wal_lsn(),pg_walfile_name(pg_current_wal_lsn()) as before_drop_lsn;
select pg_current_wal_insert_lsn(),pg_walfile_name(pg_current_wal_insert_lsn()) as before_drop_insert_lsn;
\t
select relfilenode from pg_class where relname='witnesstable';
-- Ooops #2
DROP TABLE witnesstable;
--We switch the wal to have it in archives
select pg_switch_wal();
