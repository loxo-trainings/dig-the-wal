#! /bin/bash
# pg_rewind exercise
source $(dirname $0)/../dtw_source.sh
exdir=$(dirname $0)
rewind_prim_wal=${wal_root}/11/rewind_primary
rewind_sec_wal=${wal_root}/11/rewind_secondary

## drop clusters if exists
pg_dropcluster 11 rewind_secondary --stop
pg_dropcluster 11 rewind_primary --stop

## Create clusters 
pg_createcluster 11 rewind_primary -o archive_mode='on' -o archive_command="cp %p ${rewind_prim_wal}/%f" -- -k 
pg_createcluster 11 rewind_secondary -o archive_mode='on' -o archive_command="cp %p ${rewind_sec_wal}/%f"  -- -k

cp ${scriptroot}/system/pg_hba.conf /etc/postgresql/11/rewind_primary/
cp ${scriptroot}/system/pg_hba.conf /etc/postgresql/11/rewind_secondary/
mkdir -p ${rewind_prim_wal}
rm -f  ${rewind_prim_wal}/*
mkdir -p ${rewind_sec_wal}
rm -f ${rewind_sec_wal}/*


pg_ctlcluster 11 rewind_primary start

## Retrieve clusters port
PRIMPORT=$(pg_lsclusters  11 rewind_primary -h|awk '{print $3}')
SECPORT=$(pg_lsclusters  11 rewind_secondary -h|awk '{print $3}')
SECDATA=${pgroot}/11/rewind_secondary

## Create replication user
psql --cluster 11/rewind_primary -c "CREATE USER replication WITH replication PASSWORD 'test';"
rm -rf ${SECDATA}/*
## Backup primary into secondary 
/usr/lib/postgresql/11/bin/pg_basebackup -D ${SECDATA} -p ${PRIMPORT} --wal-method=stream -R -P

## Prepare secondary to recover as standby
cat > ${SECDATA}/recovery.conf << EOF  
primary_conninfo='port=${PRIMPORT} user=replication password=test'
restore_command='cp ${rewind_prim_wal}/%f %p'
standby_mode='on'
EOF

## Launch standby
pg_ctlcluster 11 rewind_secondary start

## Run SQLScript to create database and tables
psql --cluster 11/rewind_primary -f ${exdir}/04_1_create_db_and_tables.sql
psql --cluster 11/rewind_primary splitbraindb -f ${exdir}/04_2_feed_db.sql

## Promote secondary 
pg_ctlcluster 11 rewind_secondary promote

## Create a splitbrain situation===
## First insert in secondary
psql --cluster 11/rewind_secondary splitbraindb -f ${exdir}/04_2_feed_db.sql

## The insert in prior primary
psql --cluster 11/rewind_primary   splitbraindb -f ${exdir}/04_2_feed_db.sql

echo "Splitbrain situation created"
echo "Primary:"
psql --cluster 11/rewind_primary   splitbraindb -c "SELECT  t1count,  t2count, t3count from (select count(*)t1count from t1) as t1count, (select count(*) t2count from t2)as t2count, (select count(*)t3count from t3) as t3count;"
echo "Secondary:"
psql --cluster 11/rewind_secondary splitbraindb -c "SELECT  t1count,  t2count, t3count from (select count(*)t1count from t1) as t1count, (select count(*) t2count from t2)as t2count, (select count(*)t3count from t3) as t3count;"
