DROP DATABASE IF EXISTS splitbraindb;
CREATE DATABASE splitbraindb;
\c splitbraindb
CREATE TABLE t1(id bigserial primary key, t1text text not null);
CREATE TABLE t2(id bigserial primary key, t2text text not null);
CREATE TABLE t3(id bigserial primary key, t3text text not null);

