#! /bin/bash
# create instance for first exercise
echo "+--------------------------+"
echo "| Launch clusters creation |"
echo "+--------------------------+"
basedir=$(dirname $0)
${basedir}/Exercise_1/01_0_create_instances.sh
echo "+-----------------+"
echo "| Cluster created |"
echo "+-----------------+"
