#! /bin/bash
source $(dirname $0)/../dtw_source.sh
echo "================ EXERCISE 1 : INITIALIZATION ================"
# Minimal level
pg_createcluster 11 minimal -o wal_level='minimal' \
  -o max_wal_senders=0 --start -- -k

# Replica level
replica_wal="${wal_root}/11/replica"
echo "Creating ${replica_wal}"
mkdir -p ${replica_wal}
echo "Creating ${backup_root}/11/replica"
mkdir -p ${backup_root}/11/replica
pg_createcluster 11 replica -o archive_mode='on' \
  -o archive_command="cp %p ${replica_wal}/%f" \
  --start -- -k

# Logical level
echo "Creating ${logical_wal}"
logical_wal="${wal_root}/11/logical"
mkdir -p ${logical_wal}
echo "Creating ${backup_root}/11/logical"
mkdir -p ${backup_root}/11/logical
pg_createcluster 11 logical -o archive_mode='on' \
  -o archive_command="cp %p ${logical_wal}/%f" \
  -o wal_level=logical --start -- -k
echo "================ EXERCISE 1 : INITIALIZED ================"
