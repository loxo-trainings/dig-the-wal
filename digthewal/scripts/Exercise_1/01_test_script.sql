-- This script aims so simulate db creation C(nor R)UD activity then db drop
\a
\x
\t
\set QUIET on
DROP DATABASE IF EXISTS testdb;
-- Identify each part of the process with LSN.
SELECT pg_current_wal_lsn() as start_lsn \gset
CREATE DATABASE testdb;
\c testdb;
CREATE TABLE t1(
	id bigserial PRIMARY KEY,
	data text NOT NULL
);

SELECT pg_current_wal_lsn() as start_dml_lsn \gset
INSERT INTO t1 SELECT x, md5(''||random()*extract(epoch from current_timestamp)) FROM generate_series(1,1000) as x(x);

SELECT pg_current_wal_lsn() as delete_lsn \gset
DELETE FROM t1 WHERE id = 500;

SELECT pg_current_wal_lsn() as update_lsn \gset
UPDATE t1 SET data = 'test' WHERE id=1000;

SELECT pg_current_wal_lsn() as stop_dml_lsn \gset
-- We dont't drop table or database, we'll do it when acquainted with wal
--DROP TABLE t1;
\c postgres
--DROP DATABASE testdb;
SELECT pg_current_wal_lsn() as stop_lsn \gset
select pg_walfile_name(:'start_lsn') as startwal \gset
SELECT pg_walfile_name(:'start_dml_lsn') as startdmlwal \gset
select pg_walfile_name(:'stop_lsn') as stopwal \gset
SELECT pg_walfile_name(:'stop_dml_lsn') as stopdmlwal \gset
SELECT pg_walfile_name(:'update_lsn') as updatewal \gset
select pg_walfile_name(:'delete_lsn') as deletewal \gset

select setting as clustername from pg_settings where name='data_directory' \gset

\echo # Full WAL decoded
\echo /usr/lib/postgresql/11/bin/pg_waldump -s :start_lsn  -e :stop_lsn  -p :'clustername' -b -z :startwal :stopwal | awk '''{if ($2!=0) print $0}'''
\echo # CREATE database and table  WAL decoded
\echo /usr/lib/postgresql/11/bin/pg_waldump -s :start_lsn  -e :start_dml_lsn  -p :'clustername' -b -z :startwal :stopwal | awk '''{if ($2!=0) print $0}'''
\echo # Full DML partition
\echo /usr/lib/postgresql/11/bin/pg_waldump -s :start_dml_lsn  -e :stop_dml_lsn  -p :'clustername' -b -z :startdmlwal :stopdmlwal | awk '''{if ($2!=0) print $0}'''
\echo #------ Breakdown ---------
\echo # INSERT  partition
\echo /usr/lib/postgresql/11/bin/pg_waldump -s :start_dml_lsn  -e :delete_lsn  -p :'clustername' -b -z :startdmlwal :deletewal | awk '''{if ($2!=0) print $0}'''

\echo # DELETE  partition
\echo /usr/lib/postgresql/11/bin/pg_waldump -s :delete_lsn  -e :update_lsn  -p :'clustername' -b -z :deletewal :updatewal | awk '''{if ($2!=0) print $0}'''

\echo # UPDATE partition
\echo /usr/lib/postgresql/11/bin/pg_waldump -s :update_lsn  -e :stop_dml_lsn   -p :'clustername' -b -z :updatewal :stopdmlwal | awk '''{if ($2!=0) print $0}'''
