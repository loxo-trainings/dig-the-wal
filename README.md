# Dig the WAL
This project aims to build a Virtualbox VM using vagrant and provisioning

# History
This VM was first presented at PGConf Europe 2018 in Lisbon for the training "Dig The WAL"

# Disclaimer
This VM is only for education purpose. It *should* not be used for any other usage than learning/teaching.

# Operating

Just have vagrant and a compatible virtualbox installed and run :
    
    vagrant up

You can log into the VM using

    ssh digthewal@10.0.0.20

Password for digthewal user is `digit!`. This user can sudo.

Exercises scripts are located in `/var/lib/postgresql/digthewal/scripts` and are meant to be launched by `postgres` user
