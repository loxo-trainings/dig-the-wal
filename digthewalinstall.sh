echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main 11" > /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt-get update
apt-get install -y postgresql-11 vim ntpdate
ntpdate europe.pool.ntp.org
if ! $(id digthewal); then
  useradd -m -s /bin/bash  digthewal
  usermod -G sudo -a  digthewal
  echo 'digthewal:digit!'|chpasswd
fi
sed -i.back 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart ssh
echo "Root provisionning done"
echo "Installing scripts"
cp -a /vagrant/digthewal /var/lib/postgresql/
chown -R postgres /var/lib/postgresql/digthewal
find /var/lib/postgresql/digthewal -name "*.sh" -exec chmod 750 \{\} \;
echo "Executing first script -----------------------------------------"
su -c "/var/lib/postgresql/digthewal/scripts/00_create_world.sh" postgres
su -c "pg_lsclusters" postgres
